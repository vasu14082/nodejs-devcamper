const express = require('express');
const dotenv = require('dotenv');
var morgan = require('morgan')

//Routes files
const bootcamps = require('./routes/bootcamps.js');

// Load env vars
dotenv.config({ path: './config/config.env' });

const app = express();

//Dev Logging Middleware
if( process.env.NODE_ENV === 'development'){
	app.use(morgan('dev'));
}

//mount route
app.use('/api/v1/bootcamps', bootcamps);

const PORT = process.env.PORT || 5000;

const server = app.listen(
  PORT,
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on port ${PORT}`
  )
);